from menu import Menu
from location import Location
from data import LOCATIONS

class Game:
    def __init__( self ):
        self.locationList = {}
    
    def Run( self ):
        self.Setup()
        done = False
        while not done:
            Menu.ClearScreen()
            Menu.Header( self.currentLocation.name )
            self.currentLocation.Display()
            self.CommandList();

            choice = raw_input( " >> " );
            choice = choice.lower()

            if ( choice == "w" or choice == "a" or choice == "s" or choice == "d" ):
                self.Move( choice )

    def CommandList( self ):
        print( "" )
        print( "\t Movi:            Agoj:" )
        print( "\t     [W]          [P]reni     [F]rapi" )
        print( "\t [A] [S] [D]      [R]igardi   [I]nventaro" )
        print( "" )

    def Move( self, key ):
        if ( key == "w" and self.currentLocation.neighbors["north"] is not None ):
            self.currentLocation = self.currentLocation.neighbors["north"]
            
        elif ( key == "s" and self.currentLocation.neighbors["south"] is not None ):
            self.currentLocation = self.currentLocation.neighbors["south"]
            
        elif ( key == "a" and self.currentLocation.neighbors["west"] is not None ):
            self.currentLocation = self.currentLocation.neighbors["west"]
            
        elif ( key == "d" and self.currentLocation.neighbors["east"] is not None ):
            self.currentLocation = self.currentLocation.neighbors["east"]

    def Setup( self ):
        bob = Location()
        bob.name = "Arbaro"
        bob.description = "Estas arbaro!"
        bob.neighbors["north"] = bob
        
        self.currentLocation = bob
        self.ParseLocations()

    def ParseLocations( self ):
        # Create locations
        for key, loc in LOCATIONS.items():
            l = Location()
            l.name = loc["name"]
            l.description = loc["description"]
            if ( loc.has_key( "starting location" ) ):
                self.currentLocation = l
                
            self.locationList[ key ] = l

        # Set neighbors
        directions = [ "north", "south", "east", "west" ]
        for key, loc in LOCATIONS.items():
            for d in directions:
                
                if ( loc["neighbors"].has_key( d ) ):
                    nKey = loc["neighbors"][ d ]
                    self.locationList[ key ].neighbors[ d ] = self.locationList[ nKey ]
               






