from __future__ import print_function
import subprocess as sp
import os


class Menu:
    @staticmethod
    def DrawHorizontalBar( width, symbol='-' ):
        for i in range( width ):
            print( symbol, end="" )
            
    @staticmethod
    def Header( text ):
        Menu.DrawHorizontalBar( 80 )
        print( "" )
        label = "| " + text + " |"
        print( label )
        Menu.DrawHorizontalBar( len( label )-1 )
        print( "" )

    @staticmethod
    def ClearScreen():
        if ( os.name == "posix" ):
            sp.call( 'clear',shell=True )
        else:
            sp.call( 'cls',shell=True )
