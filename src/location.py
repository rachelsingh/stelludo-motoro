from __future__ import print_function

class Location:
    def __init__( self ):
        self.name = ""
        self.description = ""
        self.neighbors = { "north" : None, "south" : None, "east" : None, "west" : None }

    def Display( self ):
        print( "\n\t " + self.description )

        print( "\n\t Vi povas iri: \t", end="" )

        if ( self.neighbors["north"] is not None ):
            print( "Norden  ", end="" )
            
        if ( self.neighbors["south"] is not None ):
            print( "Suden  ", end="" )
            
        if ( self.neighbors["east"] is not None ):
            print( "Orienten  ", end="" )
            
        if ( self.neighbors["west"] is not None ):
            print( "Okcidenten  ", end="" )

        print( "" )
