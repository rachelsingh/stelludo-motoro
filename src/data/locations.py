#!/usr/bin/python
# -*- coding: utf-8 -*-

LOCATIONS = {}

LOCATIONS[ "vilaĝo" ] = {
    "name"                  : "Verda Vilaĝo",
    "description"           : "Tiu vilaĝo estas malgranda, sed la personoj estas amikeblaj.",
    "starting location"     : True,
    "neighbors" : {
        "north" : "albergo",
        "east" : "ĝardeno",
    }
}

LOCATIONS[ "albergo" ] = {
    "name"                  : "Albergo",
    "description"           : "Vi povas dormi ĉi tie... se vi havas monon.",
    "neighbors" : {
        "south" : "vilaĝo",
    }
}

LOCATIONS[ "ĝardeno" ] = {
    "name"                  : "Ĝardeno",
    "description"           : "La floroj en la ĝardeno estas mortaj.",
    "neighbors" : {
        "west" : "vilaĝo",
    }
}

