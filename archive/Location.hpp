#ifndef _LOCATION_HPP
#define _LOCATION_HPP

#include <iostream>
#include <string>
using namespace std;

enum Directions { NORTH=0, EAST=1, SOUTH=2, WEST=3 };
string GetName( Directions dir );

struct Location
{
    string name;
    string description;
    Location* ptrNeighbors[4];

    Location();
    void Display();
};

#endif
