#include "Game.hpp"

Game::Game()
{
    m_ptrCurrent = nullptr;
    Setup();
}

void Game::Run()
{
    char choice;

    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( m_ptrCurrent->name );
        m_ptrCurrent->Display();
        CommandList();

        cout << " >> ";
        cin >> choice;
        choice = tolower( choice );

        cout << endl;

        if ( choice == 'w' || choice == 'a' || choice == 's' || choice == 'd' )
        {
        }
    }
}

void Game::Move( char dir )
{
    if ( dir == 'w' && m_ptrCurrent->ptrNeighbors[NORTH] != nullptr )
    {
        m_ptrCurrent = m_ptrCurrent->ptrNeighbors[NORTH];
    }
    else if ( dir == 's' && m_ptrCurrent->ptrNeighbors[SOUTH] != nullptr )
    {
        m_ptrCurrent = m_ptrCurrent->ptrNeighbors[SOUTH];
    }
    else if ( dir == 'a' && m_ptrCurrent->ptrNeighbors[WEST] != nullptr )
    {
        m_ptrCurrent = m_ptrCurrent->ptrNeighbors[WEST];
    }
    else if ( dir == 'd' && m_ptrCurrent->ptrNeighbors[EAST] != nullptr )
    {
        m_ptrCurrent = m_ptrCurrent->ptrNeighbors[EAST];
    }
}

void Game::CommandList()
{
    cout << endl;
    cout << "\t Movi:           Agoj:" << endl;
    cout << "\t     [W]         [P]reni     [F]rapi" << endl;
    cout << "\t [A] [S] [D]     [R]igardi   [I]nventaro" << endl;
    cout << endl;
}

void Game::Setup()
{
    Location bob;
    bob.name = "Bela arbaro";
    bob.description = "La arbaro estas malluma sed belega.";
    bob.ptrNeighbors[0] = &bob;
    m_locations.push_back( bob );
    m_ptrCurrent = &m_locations[0];
}
