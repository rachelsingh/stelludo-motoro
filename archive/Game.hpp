#ifndef _GAME_HPP
#define _GAME_HPP

#include <iostream>
#include <vector>
using namespace std;

#include "Location.hpp"
#include "Menu.hpp"

class Game
{
    public:
    Game();
    void Run();
    void CommandList();

    private:
    void Setup();
    void Move( char dir );

    vector<Location> m_locations;
    Location* m_ptrCurrent;
};

#endif
