#include "Location.hpp"

string GetName( Directions dir )
{
    if ( dir == NORTH )         { return "Norden"; }
    else if ( dir == SOUTH )    { return "Suden"; }
    else if ( dir == EAST )     { return "Orienten"; }
    else if ( dir == WEST )     { return "Okcidenten"; }
    else                        { return "KIO ESTAS TIO?!"; }
}

Location::Location()
{
    for ( int i = 0; i < 4; i++ )
    {
        ptrNeighbors[i] = nullptr;
    }
}

void Location::Display()
{
    cout << "\t " << description << endl;
    cout << endl;
    cout << "\t Vi povas iri: ";

    bool nowhere = true;
    for ( int i = 0; i < 4; i++ )
    {
        if ( ptrNeighbors[i] != nullptr )
        {
            cout << GetName( Directions( i ) ) << " ";
            nowhere = false;
        }
    }

    if ( nowhere )
    {
        cout << "Nenien" << endl;
    }

    cout << endl;
}
